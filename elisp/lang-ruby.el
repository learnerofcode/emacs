;;; lang-ruby.el

(use-package ruby-mode
  :config
  (add-hook 'ruby-mode-hook 'flycheck-mode))

(use-package ruby-end
  :bind (:map ruby-mode-map
              ("C-j" . ruby-end-return))
  :config
  (add-hook 'ruby-mode-hook 'ruby-end-mode)
  :diminish ruby-end-mode)

(use-package robe
  :config
  (add-hook 'ruby-mode-hook 'robe-mode)
  (add-to-list 'company-backends 'company-robe)
  :diminish robe-mode)

(use-package inf-ruby
  :init
  (add-hook 'ruby-mode-hook 'inf-ruby-minor-mode))

(use-package rinari
  :init
  (add-hook 'ruby-mode-hook 'rinari-launch)
  :diminish rinari-minor-mode)

(use-package rspec-mode
  :bind (:map ruby-mode-map
              ("C-c v" . rspec-verify)
              ("C-c a" . rspec-verify-all)
              ("C-c s" . rspec-verify-single)
              ("C-c t" . rspec-toggle-spec-and-target))
  :diminish rspec-mode)

(provide 'lang-ruby)
