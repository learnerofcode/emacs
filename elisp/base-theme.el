;;; base-theme.el

(use-package doom-themes
  :after treemacs
  :config
  (load-theme 'doom-horizon t)

  (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
  (doom-themes-treemacs-config))

(add-to-list 'default-frame-alist '(font . "jetbrains mono 13"))
(add-to-list 'default-frame-alist '(width . 96))
(add-to-list 'default-frame-alist '(height . 48))

(setq-default line-spacing 3)

(provide 'base-theme)
