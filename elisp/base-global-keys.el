;;; base-global-keys.el

(global-set-key "\C-xp" 'ltd-ivy-find-project)

(global-set-key "\C-xK" 'ltd-kill-buffer-and-file)

(global-set-key "\C-xm" 'eshell)
(global-set-key (kbd "C-x C-m") 'shell)

(global-set-key (kbd "C-c y") 'bury-buffer)
(global-set-key (kbd "C-c r") 'revert-buffer)

(global-set-key (kbd "M-i") 'ltd-insert-short-tab)
(global-set-key (kbd "M-I") 'ltd-insert-tab)

(global-set-key "\C-j" 'newline-and-indent)
(global-set-key "\C-cj" 'join-line)
(global-set-key "\C-w" 'ltd-kill-region-or-backward-kill-word)
(global-set-key "\C-cd" 'ltd-duplicate-line-or-region)

(global-set-key (kbd "s-+") 'text-scale-adjust)
(global-set-key (kbd "s--") 'text-scale-adjust)
(global-set-key (kbd "s-0") 'text-scale-adjust)
(global-set-key (kbd "s-=") 'text-scale-adjust)

(global-set-key (kbd "M-RET") 'toggle-frame-fullscreen)
(global-set-key (kbd "M-/") 'hippie-expand)

(provide 'base-global-keys)
