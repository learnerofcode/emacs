;;; lang-javascript.el

(use-package js2-mode
  :mode
  ("\\.js$" . js2-jsx-mode)
  ("\\.jsx$" . js2-jsx-mode)

  :config
  (setq js2-basic-offset 2)

  (setq
   js2-strict-missing-semi-warning nil
   js2-strict-trailing-comma-warning nil
   js2-mode-show-parse-errors nil)

  (setq js2-global-externs '("JSON" "React" "__dirname" "assert"
                             "beforeAll" "beforeEach" "clearInterval"
                             "clearTimeout" "console" "describe"
                             "expect" "it" "localStorage" "location"
                             "module" "process" "require"
                             "setInterval" "setTimeout" "sinon" "test"
                             "window"))

  (add-hook 'js2-mode-hook 'flycheck-mode))

(use-package js-comint
  :config
  (defun inferior-js-mode-hook-setup ()
    (add-hook 'comint-output-filter-functions 'js-comint-process-output))

  (add-hook 'inferior-js-mode-hook 'inferior-js-mode-hook-setup))

(use-package typescript-mode
  :mode
  ("\\.ts$" . typescript-mode)
  ; ("\\.tsx$" . typescript-mode)

  :config
  (setq typescript-indent-level 2))

(use-package web-mode
  :after flycheck
  :mode
  ("\\.tsx$" . web-mode)
  :config
  (flycheck-add-mode 'typescript-tslint 'web-mode))

(use-package tide
  :after (typescript-mode web-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
         (web-mode . tide-setup)
         (web-mode . tide-hl-identifier-mode))
  :diminish tide-mode)

(use-package add-node-modules-path
  :hook ((js2-jsx-mode . add-node-modules-path)
         (typescript-mode . add-node-modules-path)
         (web-mode . add-node-modules-path)))

(use-package prettier-js
  :config
  (setq prettier-js-args '(
                           "--print-width" "120"
                           "--single-quote" "true"
                           "--bracket-spacing" "false"
                           ))
  :hook ((js2-jsx-mode . prettier-js-mode)
         (typescript-mode . prettier-js-mode))
  :diminish prettier-js-mode)

(use-package vue-mode
  :mode
  ("\\.vue$" . vue-mode))

(use-package dart-mode
  :mode
  ("\\.dart$" . dart-mode))

(provide 'lang-javascript)
