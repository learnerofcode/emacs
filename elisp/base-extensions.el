;;; base-extensions.el

(use-package ag)

(use-package all-the-icons
  :config
  (add-to-list 'all-the-icons-icon-alist
               '("\\.mdx$" all-the-icons-octicon "markdown" :height 1.0 :v-adjust -0.1 :face all-the-icons-blue-alt))
  (add-to-list 'all-the-icons-icon-alist
               '("\\.jsx$" all-the-icons-alltheicon "javascript" :height 1.0 :v-adjust -0.1 :face all-the-icons-blue-alt))
  (add-to-list 'all-the-icons-icon-alist
               '("\\.tsx$" all-the-icons-fileicon "typescript" :height 1.0 :v-adjust -0.1 :face all-the-icons-blue-alt)))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package anzu
  :defer 1
  :bind
  (([remap query-replace] . anzu-query-replace)
   ([remap query-replace-regexp] . anzu-query-replace-regexp)
   :map isearch-mode-map
   ([remap isearch-query-replace] . anzu-isearch-query-replace)
   ([remap isearch-query-replace-regexp] . anzu-isearch-query-replace-regexp))
  :config
  (global-anzu-mode +1)
  :diminish anzu-mode)

(use-package autorevert
  :init (global-auto-revert-mode +1)
  :config
  (setq
   auto-revert-verbose nil
   global-auto-revert-non-file-buffers t)
  :diminish auto-revert-mode)

(use-package avy
  :bind
  ("C-c SPC" . avy-goto-char)
  :config
  (setq avy-case-fold-search nil))

(use-package beacon
  :init (beacon-mode +1))

(use-package company
  :config
  (setq company-idle-delay 0.1)
  (setq company-tooltip-align-annotations t)

  (add-hook 'after-init-hook 'global-company-mode)
  :diminish company-mode)

(use-package company-lsp
  :after (company lsp-mode)
  :config
  (push 'company-lsp company-backends))

(use-package dockerfile-mode
  :mode ("Dockerfile$" . dockerfile-mode))

(use-package diminish)

(use-package feature-mode
  :mode ("\\.feature$" . feature-mode))

(use-package flycheck
  :hook (after-init . global-flycheck-mode)
  :config
  (setq flycheck-mode-line-prefix "¢")
  (setq flycheck-indication-mode 'right-fringe)
  (setq flycheck-check-syntax-automatically '(save mode-enabled))

  (if (display-graphic-p)
      (use-package flycheck-pos-tip
        :hook (global-flycheck-mode . flycheck-pos-tip-mode)
        :config (setq flycheck-pos-tip-timeout 30)))

  (use-package avy-flycheck
    :hook (global-flycheck-mode . avy-flycheck-setup)))

(use-package flx)

(use-package exec-path-from-shell
  :config
  (when (memq window-system '(mac ns))
    (exec-path-from-shell-initialize)))

(use-package expand-region
  :bind
  ("C-=" . er/expand-region))

(use-package git-gutter
  :config
  (global-git-gutter-mode +1)
  :diminish git-gutter-mode)

(use-package ibuffer
  :bind (([remap list-buffers] . ibuffer))
  ;; Show VC Status in ibuffer
  :config
  (setq
   ibuffer-formats
   '((mark modified read-only vc-status-mini " "
           (name 18 18 :left :elide)
           " "
           (size 9 -1 :right)
           " "
           (mode 16 16 :left :elide)
           " "
           (vc-status 16 16 :left)
           " "
           filename-and-process)
     (mark modified read-only " "
           (name 18 18 :left :elide)
           " "
           (size 9 -1 :right)
           " "
           (mode 16 16 :left :elide)
           " " filename-and-process)
     (mark " " (name 16 -1) " " filename))))

(use-package ibuffer-vc
  :defer t
  :init (add-hook 'ibuffer-hook
                  (lambda ()
                    (ibuffer-vc-set-filter-groups-by-vc-root)
                    (unless (eq ibuffer-sorting-mode 'alphabetic)
                      (ibuffer-do-sort-by-alphabetic)))))

(use-package counsel
  :bind
  ("M-x" . counsel-M-x)
  ("C-x C-f" . counsel-find-file)
  ("M-y" . counsel-yank-pop))

(use-package counsel-projectile
  :after projectile
  :config
  (counsel-projectile-mode +1))

(use-package ivy
  :bind
  ("C-x s" . swiper)
  ("C-x C-r" . ivy-resume)
  ("C-x b" . ivy-switch-buffer)
  :config
  (ivy-mode +1)
  (setq
   ivy-use-virtual-buffers t
   ivy-re-builders-alist '((t . ivy--regex-fuzzy)))
  (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
  :diminish ivy-mode)

(use-package magit
  :bind
  ("C-c g" . magit-status)
  :config
  (setq magit-completing-read-function 'ivy-completing-read))

(use-package multiple-cursors
  :bind
  ("C-S-c C-S-c" . mc/edit-lines)
  ("C-." . mc/mark-next-like-this)
  ("C-," . mc/mark-previous-like-this)
  ("C->" . mc/mark-all-like-this))

(use-package org
  :bind
  ("C-c l" . org-store-link)
  ("C-c a" . org-agenda)
  :config
  (setq org-directory "~/org-files"
        org-default-notes-file (concat org-directory "/todo.org")))

(use-package org-bullets
  :config
  (setq org-hide-leading-stars t)
  (add-hook 'org-mode-hook
            (lambda ()
              (org-bullets-mode t))))

(use-package org-pomodoro)

(use-package projectile
  :config
  (setq projectile-known-projects-file
        (concat temp-dir "/projectile-bookmarks.eld"))

  (setq projectile-completion-system 'ivy)

  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

  :diminish projectile-mode)

(use-package doom-modeline
  :init (doom-modeline-mode +1))

(use-package recentf
  :config
  (setq recentf-save-file (recentf-expand-file-name (concat temp-dir "/recentf")))
  (recentf-mode +1))

(use-package smartparens
  :config
  (smartparens-global-mode +1)
  :diminish smartparens-mode)

(use-package smex
  :config
  (setq smex-save-file (concat temp-dir "/smex-items")))

(use-package undo-tree
  :config
  (setq undo-tree-auto-save-history nil)
  (global-undo-tree-mode +1)
  :diminish undo-tree-mode)

(use-package which-key
  :config
  (which-key-mode)
  :diminish which-key-mode)

(use-package move-text
  :bind
  ("M-<up>" . move-text-up)
  ("M-<down>" . move-text-down))

(use-package lsp-mode
  :commands lsp
  :config
  (setq lsp-session-file (concat temp-dir "/.lsp-session-v1")))

(use-package lsp-ui)

(use-package hippie-exp
  :bind (("M-/" . hippie-expand))
  :config
  (setq hippie-expand-try-functions-list
        '(yas-hippie-try-expand
          try-expand-dabbrev
          try-expand-dabbrev-all-buffers
          try-expand-dabbrev-from-kill
          try-complete-file-name-partially
          try-complete-file-name
          try-expand-all-abbrevs
          try-expand-list
          try-complete-lisp-symbol-partially
          try-complete-lisp-symbol)))

(use-package server
  :if (not noninteractive)
  :defer t
  :init (server-mode))

(use-package shell
  :bind (:map comint-mode-map
              ("<up>" . comint-previous-input)
              ("<down>" . comint-next-input)))

(use-package treemacs
  :config
  (setq treemacs-persist-file (concat temp-dir "/treemacs-persist")))

(use-package vimish-fold
  :config
  (setq vimish-fold-dir (concat temp-dir "/vimish-fold")))

(use-package yasnippet
  :config
  (yas-global-mode +1)
  :diminish yas-minor-mode)

(use-package yasnippet-snippets
  :after yasnippet)

(use-package yaml-mode
  :mode ("\\.ya?ml$" . yaml-mode))

(provide 'base-extensions)
