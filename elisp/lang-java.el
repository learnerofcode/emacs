(use-package lsp-java
  :requires lsp-mode
  :config
  (add-hook 'java-mode-hook 'lsp)

  (defun java-format-on-save-hook()
    (when (eq major-mode 'java-mode)
      (lsp-format-buffer)))
  (add-hook 'after-save-hook 'java-format-on-save-hook))

(use-package google-c-style
  :config
  (add-hook 'java-mode-hook 'google-set-c-style))

(provide 'lang-java)
