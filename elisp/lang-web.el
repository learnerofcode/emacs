;;; lang-web.el

(use-package web-mode
  :mode
  ("\\.erb$" . web-mode)
  ("\\.html$" . web-mode)
  ("\\.hbs$" . web-mode)
  ("\\.handlebars$" . web-mode)
  ("\\.liquid$" . web-mode)
  ("\\.php$" . web-mode)

  :config
  (setq
   web-mode-enable-auto-quoting nil
   web-mode-markup-indent-offset 2
   web-mode-css-indent-offset 2
   web-mode-code-indent-offset 2
   web-mode-style-padding 2
   web-mode-script-padding 2
   web-mode-enable-current-element-highlight t))

(use-package emmet-mode
  :bind (:map emmet-mode-keymap
              ("C-<return>" . emmet-expand-yas)
              ("C-j" . nil))
  :init
  (add-hook 'web-mode-hook 'emmet-mode)
  :diminish emmet-mode)

(use-package css-mode
  :config
  (setq css-indent-offset 2))

(provide 'lang-web)
