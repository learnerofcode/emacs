(use-package clojure-mode
  :hook ((clojure-mode . eldoc-mode))
  :config
  (setq clojure-indent-style 'align-arguments)
  (setq clojure-align-forms-automatically t))

(use-package flycheck-clj-kondo
  :after flycheck)

(use-package rainbow-delimiters)

;; (use-package lispy
;;   :hook ((emacs-lisp-mode . lispy-mode)
;;          (clojure-mode . lispy-mode))
;;   :config
;;   (setq lispy-close-quotes-at-end-p t)
;;   (add-hook 'lispy-mode-hook 'rainbow-delimiters-mode)
;;   (add-hook 'lispy-mode-hook 'turn-off-smartparens-mode))

;; (use-package parinfer
;;   :bind (:map parinfer-mode-map
;;               ("C-," . parinfer-toggle-mode)
;;               ("\"" . paredit-doublequote))
;;   :init
;;   (progn
;;     (setq parinfer-extensions
;;           '(defaults       ; should be included.
;;              pretty-parens ; different paren styles for different modes.
;;              paredit       ; Introduce some paredit commands.
;;              smart-tab     ; C-b & C-f jump positions and smart shift with tab & S-tab.
;;              smart-yank))  ; Yank behavior depend on mode.
;;     (add-hook 'clojure-mode-hook #'parinfer-mode)
;;     (add-hook 'emacs-lisp-mode-hook #'parinfer-mode)
;;     (add-hook 'lisp-mode-hook #'parinfer-mode)
;;     (add-hook 'parinfer-mode-hook #'turn-off-smartparens-mode)))

;; (use-package parinfer-rust-mode
;;   :bind
;;   (("C-," . parinfer-rust-toggle-paren-mode)
;;    ("\"" . paredit-doublequote))
;;   :init
;;   (progn
;;     (setq parinfer-rust-auto-download t)
;;     (add-hook 'clojure-mode-hook #'parinfer-rust-mode)
;;     (add-hook 'emacs-lisp-mode-hook #'parinfer-rust-mode)
;;     (add-hook 'lisp-mode-hook #'parinfer-rust-mode)
;;     (add-hook 'parinfer-rust-mode-hook #'rainbow-delimiters-mode)
;;     (add-hook 'parinfer-rust-mode-hook #'turn-off-smartparens-mode)))

(use-package paredit
  :init
  (progn
    (add-hook 'clojure-mode-hook #'paredit-mode)
    (add-hook 'emacs-lisp-mode-hook #'paredit-mode)
    (add-hook 'lisp-mode-hook #'paredit-mode)
    (add-hook 'paredit-mode-hook #'rainbow-delimiters-mode)
    (add-hook 'paredit-mode-hook #'turn-off-smartparens-mode)))

(use-package cider
  :config
  (setq cider-enhanced-cljs-completion-p nil)
  (setq cider-repl-pop-to-buffer-on-connect 'display-only)
  (setq cider-repl-display-help-banner nil)
  (setq cider-repl-result-prefix ";; => ")

  (setq cider-repl-history-display-duplicates nil
        cider-repl-history-display-style 'one-line
        cider-repl-history-file (concat temp-dir "/cider-repl-history")
        cider-repl-history-highlight-current-entry t
        cider-repl-history-quit-action 'delete-and-restore
        cider-repl-history-highlight-inserted-item t
        cider-repl-history-size 1000))

(use-package clj-refactor
  :after (clojure-mode cider)
  :config
  (setq cljr-warn-on-eval nil)
  :hook ((clojure-mode . clj-refactor-mode)))

(use-package zprint-mode)

(provide 'lang-clojure)
