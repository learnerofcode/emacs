;;; base-functions.el

(defun ltd-ivy-find-project ()
  "Open a project root."
  (interactive)
  (find-file
   (concat "~/Projects/" (ivy-completing-read "Project: "
                                              (directory-files "~/Projects/" nil "^[^.]")))))

(defun ltd-kill-buffer-and-file ()
  "Kill current buffer and the file it is visiting."
  (interactive)
  (let ((buffer (current-buffer))
        (name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Current buffer is not visiting a file!")
      (when (yes-or-no-p "Sure you want to delete this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "Buffer killed, file deleted.")))))

(defun ltd-rename-buffer-and-file (new-name)
  "Rename current buffer and the file it is visiting."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (error "Current buffer is not visiting a file!")
      (if (get-buffer new-name)
          (error "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file name new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File renamed to '%s'" new-name))))))

(defun ddg ()
  "Search for an input string or the active region on duckduckgo."
  (interactive)
  (browse-url
   (concat
    "https://duckduckgo.com/?q="
    (if mark-active
        (buffer-substring (region-beginning) (region-end))
      (read-string "Search: ")))))

(defun ltd-duplicate-line-or-region ()
  "Duplicate active region if any.  Otherwise, duplicate the current line."
  (interactive)
  (if (region-active-p)
      (ltd-duplicate-region)
    (ltd-duplicate-line)))

(defun ltd-duplicate-line ()
  "Duplicate current line."
  (interactive)
  (beginning-of-line)
  (copy-region-as-kill (point) (progn (end-of-line) (point)))
  (newline)
  (yank)
  (indent-according-to-mode))

(defun ltd-duplicate-region ()
  "Duplicate active region."
  (interactive)
  (copy-region-as-kill (region-beginning) (region-end))
  (goto-char (region-end))
  (yank))

(defun ltd-kill-region-or-backward-kill-word ()
  "Kill active region if any.  Otherwise, kill word backward."
  (interactive)
  (if (region-active-p)
      (kill-region (region-beginning) (region-end))
    (sp-backward-kill-word 1)))

(defun ltd-now-playing ()
  "Insert most recent Last.FM track."
  (interactive)
  (insert (shell-command-to-string "~/bin/now-playing")))

(defun ltd-insert-tab ()
  "Insert tab."
  (interactive)
  (insert "\t"))

(defun ltd-insert-short-tab ()
  "Insert two spaces."
  (interactive)
  (insert "  "))

(defun ltd-reset-theme ()
  "Reset all faces.  Use this before switching theme."
  (interactive)
  (dolist (theme custom-enabled-themes)
    (disable-theme theme)))

(defun ltd-light-theme ()
  "Load light theme."
  (interactive)
  (ltd-reset-theme)
  (load-theme 'doom-one-light t))

(defun ltd-dark-theme ()
  "Load dark theme."
  (interactive)
  (ltd-reset-theme)
  (load-theme 'doom-horizon t))

(defun ltd-unobtrusive-fringes ()
  "Make the fringe blend in background."
  (interactive)
  (set-face-attribute 'fringe nil
                      :background (face-background 'default)))

(provide 'base-functions)
