;;; base.el

;; Dir constants
(defconst private-dir (expand-file-name "private" user-emacs-directory))
(defconst temp-dir (format "%s/cache" private-dir))

;; Use-package always ensure
(require 'use-package-ensure)
(setq use-package-always-ensure t)

;; Essentials
(setq
 inhibit-startup-message    t
 initial-scratch-message    ""
 visible-bell               t
 ring-bell-function         'ignore
 kill-whole-line            t
 custom-file                "~/.emacs.d/custom.el")

(setq-default
 indent-tabs-mode  nil
 tab-width         2
 major-mode        'org-mode)

;; UTF-8
(prefer-coding-system 'utf-8)

;; Show column number in modeline
(column-number-mode 1)

;; Show matching parentheses
(show-paren-mode 1)

;; Backup settings
(setq
 history-length                   1000
 backup-inhibited                 nil
 make-backup-files                t
 auto-save-default                t
 auto-save-list-file-name         (concat temp-dir "/autosave")
 make-backup-files                t
 create-lockfiles                 nil
 backup-directory-alist          `((".*" . ,(concat temp-dir "/backup/")))
 auto-save-file-name-transforms  `((".*" ,(concat temp-dir "/auto-save-list/") t)))

(fset 'yes-or-no-p 'y-or-n-p)

;; Disable some GUI components
(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

;; Other GUI settings
(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b")))
  (tooltip-mode -1)
  (mouse-wheel-mode t)
  (blink-cursor-mode -1))

;; Delete trailing whitespace before save
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(provide 'base)
