;;; init.el

(setq package-enable-at-startup nil)

(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
        ("gnu" . "https://elpa.gnu.org/packages/")))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(add-to-list 'load-path (concat user-emacs-directory "elisp"))

(require 'base)
(require 'base-extensions)
(require 'base-functions)
(require 'base-global-keys)
(require 'base-theme)
(require 'lang-java)
(require 'lang-javascript)
(require 'lang-ruby)
(require 'lang-web)
(require 'lang-clojure)
(require 'lang-rust)

(find-file "~/Code")
